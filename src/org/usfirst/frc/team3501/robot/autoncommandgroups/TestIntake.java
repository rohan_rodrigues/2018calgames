package org.usfirst.frc.team3501.robot.autoncommandgroups;


import org.usfirst.frc.team3501.robot.Constants;
import edu.wpi.first.wpilibj.command.CommandGroup;

public class TestIntake extends CommandGroup {

  public static final double ROBOT_LENGTH = 38.0;
  public static final double SCALE = Constants.Auton.SCALE_FACTOR_TESTING;

  public static final double TO_CUBES = 84;
  public static final double HORIZ_SWITCH_DIST = 50.0;
  public static final double VERT_SWITCH_DIST = 60;

  public TestIntake() {}
}
