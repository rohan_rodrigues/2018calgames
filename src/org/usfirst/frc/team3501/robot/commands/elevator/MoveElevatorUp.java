package org.usfirst.frc.team3501.robot.commands.elevator;

import org.usfirst.frc.team3501.robot.Robot;
import org.usfirst.frc.team3501.robot.subsystems.Elevator;
import edu.wpi.first.wpilibj.command.Command;


public class MoveElevatorUp extends Command {

  private Elevator elevator = Robot.getElevator();

  private double maxTimeOut;

  /**
   * @param target the height the elevator will move to in inches
   * @param maxTimeOut the maximum time this command will be allowed to run before being cut
   */
  public MoveElevatorUp() {
    requires(elevator);
  }

  @Override
  protected void initialize() {
    elevator.setTargetElavatorPos(elevator.getHeight());
    elevator.toggleMoveToTarget(false);
    elevator.setCANTalonsCoast();
    System.out.println("Moved up");
  }

  @Override
  protected void execute() {
    elevator.setMotorValue(elevator.UP_SPEED);
    System.out.println("Elevator motor value: " + elevator.getMotorVal());
  }

  @Override
  protected boolean isFinished() {
    if (elevator.isAtTop()) {
      System.out.println("Reached top");
      return true;
    }
    return false;
  }

  @Override
  protected void end() {
    elevator.setCANTalonsBrake();
    this.elevator.stop();
    elevator.setTargetElavatorPos(elevator.getHeight());
  }

  @Override
  protected void interrupted() {
    end();
  }
}
