package org.usfirst.frc.team3501.robot.commands.intake;

import org.usfirst.frc.team3501.robot.subsystems.Intake;
import edu.wpi.first.wpilibj.command.Command;

/**
 *
 */
public class ToggleIntakePiston extends Command {
  private Intake intake = Intake.getIntake();

  public ToggleIntakePiston() {
    requires(intake);
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    System.out.println("Run toggle piston");

    // if (intake.isPistonActivated())
    // intake.setMotorValues(-intake.intakeSpeed);
    intake.setPistonActivated(!intake.isPistonActivated());
    System.out.println(intake.isPistonActivated());
    // intake.getIntakeSolenoid().set(intake.isPistonActivated());
    // intake.getIntakeSolenoidTwo().set(intake.isPistonActivated());
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {

  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return timeSinceInitialized() > 1;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
    intake.stop();
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {}
}
