package org.usfirst.frc.team3501.robot.commands.intake;

import org.usfirst.frc.team3501.robot.Robot;
import org.usfirst.frc.team3501.robot.subsystems.Intake;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;

/**
 * @author: niyatisriram
 *
 *          this command runs the intake backward, for the purposes of intaking a cube wheels roll
 *          in, towards robot center
 *
 */
public class RunIntake extends Command {

  private Intake intake = Robot.getIntake();
  double timeToRun = 0.25;
  Timer timer;

  public RunIntake() {
    requires(intake);
    timer = new Timer();
  }

  @Override
  protected void initialize() {
    timer.start();
    while (timer.get() <= timeToRun) {
      intake.setMotorValues(-intake.intakeSpeed);
    }
    intake.setPistonActivated(false);
  }

  @Override
  protected void execute() {
    // intake.setMotorValues(-intake.outtakeSpeed);
    System.out.println("Motor speed: " + intake.getEncoderPulses());
    System.out.println("Target: " + intake.getIntakeTarget());
    System.out.println(
        "______________\n Angle value: " + intake.getIntakeAngleMotor().get());
  }

  @Override
  protected boolean isFinished() {
    return true;
  }

  @Override
  protected void end() {
    intake.stop();
  }

  @Override
  protected void interrupted() {
    end();
  }
}
