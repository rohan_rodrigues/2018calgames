package org.usfirst.frc.team3501.robot.commands.intake;

import org.usfirst.frc.team3501.robot.Robot;
import org.usfirst.frc.team3501.robot.subsystems.Intake;
import edu.wpi.first.wpilibj.command.Command;

/**
 *
 */
public class MoveUp extends Command {
  private Intake intake = Robot.getIntake();

  public MoveUp() {
    requires(intake);
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    System.out.println("Moved up");
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    System.out.println("Moved up");
    intake.setAngleMotorValue(-intake.upSpeed);
    System.out.println("Angle motor value: " + intake.getAngleMotorValue());
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return false;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
    intake.setAngleMotorValue(0);
  }

  @Override
  protected void interrupted() {
    System.out.println("Interrupted");
    end();
  }
}
