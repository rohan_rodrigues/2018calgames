package org.usfirst.frc.team3501.robot.commands.intake;

import org.usfirst.frc.team3501.robot.Robot;
import org.usfirst.frc.team3501.robot.subsystems.Intake;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;

/**
 * @author: niyatisriram
 *
 *          this command runs the intake forward, for the purposes of expelling a cube wheels roll
 *          out, away from robot center
 *
 */
public class RunOuttake extends Command {

  private Intake intake = Robot.getIntake();
  double timeToRun = 0.25;
  Timer timer;

  public RunOuttake() {
    requires(intake);
    timer = new Timer();
  }

  @Override
  protected void initialize() {
    timer.start();
    while (timer.get() <= timeToRun) {
      intake.setMotorValues(intake.outtakeSpeed);
    }
    intake.setPistonActivated(true);
  }

  @Override
  protected void execute() {
    System.out.println("Motor speed: " + intake.getEncoderPulses());
    System.out.println("Target: " + intake.getIntakeTarget());
    System.out.println(
        "______________\n Angle value: " + intake.getIntakeAngleMotor().get());
  }

  @Override
  protected boolean isFinished() {
    return true;
  }

  @Override
  protected void end() {

    intake.stop();

  }

  @Override
  protected void interrupted() {
    end();
  }
}
